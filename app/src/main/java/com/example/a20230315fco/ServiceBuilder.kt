package com.example.a20230315fco

import com.google.gson.Gson
import okhttp3.OkHttpClient

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ServiceBuilder {

    private val client = OkHttpClient.Builder().build()

    //example
    //https://jsonplaceholder.typicode.com

    private val retrofit = Retrofit.Builder()
        .baseUrl("https://data.cityofnewyork.us")
        .addConverterFactory(GsonConverterFactory.create())
        .client(client)
        .build()

    fun<T> buildservice(service: Class<T>):T {
        return retrofit.create(service)
    }
}