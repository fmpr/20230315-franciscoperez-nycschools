package com.example.a20230315fco


import android.app.DownloadManager.Request
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TableLayout
import org.w3c.dom.Text
import kotlinx.*
import androidx.lifecycle.ViewModel
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.a20230315fco.databinding.ActivityMainBinding
import com.google.gson.Gson

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers


// view
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.recyclerView.layoutManager = LinearLayoutManager(this)

        val serviceGenerator = ServiceBuilder.buildservice(ServiceInterface::class.java)
        val call = serviceGenerator.getAllSchools()


        call.enqueue(object: Callback<MutableList<SchoolsModel>> {
            override fun onResponse(call: Call<MutableList<SchoolsModel>>,response: Response<MutableList<SchoolsModel>>
            ) {
                if (response.isSuccessful){
                    Log.e("response",response.body().toString())
                }
            }

            override fun onFailure(call: Call<MutableList<SchoolsModel>>, t: Throwable) {
                t.printStackTrace()
                Log.e("response",t.message.toString())
            }

        })

    } // end onCreate
}// end class



