package com.example.a20230315fco

public class Schools {

    private var school_name: String
    private var phone_number: String

    constructor(school_name: String, phone_number: String) {
        this.school_name = school_name
        this.phone_number = phone_number
    }
}
