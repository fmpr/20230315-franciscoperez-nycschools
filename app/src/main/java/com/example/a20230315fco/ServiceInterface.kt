package com.example.a20230315fco

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers

interface ServiceInterface {

    @Headers("Content-Type:application/json")
    @GET("/resource/s3k6-pzi2.json")

    //@GET("/posts")

    fun getAllSchools(): Call<MutableList<SchoolsModel>>
}