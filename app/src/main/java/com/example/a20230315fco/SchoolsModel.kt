package com.example.a20230315fco

/*
  val userId:Int? = null,
  val id:Int? = null,
  val title: String? = null,
  val body:String? = null
  */

 data class SchoolsModel(

   val school_name:String? = null,
   val dbn:String? = null,
   val phone_number:String? = null

 )